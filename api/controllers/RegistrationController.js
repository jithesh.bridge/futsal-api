/**
 * RegistrationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const shortid = require('shortid');
module.exports = {
    create: function (req, res) { 
        //@todo - validate

        //@todo - save
        let data = {};
        data.company_name = req.body.company_name;
        data.team_name = req.body.team_name;
        data.contact_person_1 = req.body.contact_person_1;
        data.contact_person_1_designation = req.body.contact_person_1_designation;
        data.contact_person_1_email = req.body.contact_person_1_email;
        data.contact_person_1_phone = req.body.contact_person_1_phone;
        data.contact_person_2 = req.body.contact_person_2;
        data.contact_person_2_designation = req.body.contact_person_2_designation;
        data.contact_person_2_email = req.body.contact_person_2_email;
        data.contact_person_2_phone = req.body.contact_person_2_phone;
        data.items = req.body.items.split(',');
        const teamId = shortid.generate(); 
        data.team_id = teamId;// shortid.generate(); 

        Registration.create(data).exec(function(error, userData) {
        //Registration.create(data).fetch().exec(function (err, userData) {
            if (error) {
              res.json({message:'DB error',error:error});
            } else {
                // setting allowed file types
                var allowedTypes = ['image/jpeg', 'image/png'];

                req.file('image').upload({
                    // don't allow the total upload size to exceed ~10MB
                    maxBytes: 10000000,
                    saveAs:function(file, cb) {
                        let extension = file.filename.split('.').pop();
                        let fileName = teamId+"."+ extension;

                        if(allowedTypes.indexOf(file.headers['content-type']) === -1) {
                            return res.badRequest('File is invalid');
                        }
                        
                        cb(null,fileName);
                    },
                    dirname: require('path').resolve(sails.config.appPath, 'uploads')
                },function whenDone(err, uploadedFiles) {
                    if (err) {
                        return res.serverError(err);
                    }
                
                    // If no files were uploaded, respond with an error.
                    if (uploadedFiles.length === 0){
                        return res.badRequest('No file was uploaded');
                    }

                    return res.json({
                        status: true,
                        message: 'Registered successfully!',
                        //files: uploadedFiles,
                        data: userData 
                    });

                });

              //res.json({message:'test'});
            }
          });


        
    },

    list: function (req, res) { 
        return Registration.find().then(function (registrations) {
            return res.json({status: true, data: registrations});
        }).catch(function (err) {
            return res.json({status: false, message: "Sorry, an error occurd - " + err});
        });
    },

    show: function (req, res) {
        let registrationId = req.param('id');
        // Registration.findOne({ where: { _id:registrationId}}).exec(function(err, model) {
        //     if(err) return res.json({status: false, message: "Sorry, an error occurd - " + err});

        //     return res.json({status: true, data: registration});
        // });

        Registration.findOne({id: registrationId}).exec(function(err, registration) { 
            if(err) return res.json({status: false, message: "Sorry, an error occurd - " + err});

            return res.json({status: true, data: registration});
        });
    },

    delete:function(req, res){
        let registrationId = req.param('id');

        //delete image


        Registration.destroy(registrationId).exec(function(err, result){
            if(err) return res.json({status: false, message: "Sorry, an error occurd - " + err});

            return res.json({status: true, message:'Registartion removed successfully'});
        });
    }


};

