/**
 * Registration.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: { type: 'string', columnName: '_id' },
    company_name: { type: 'string', required: true, },
    team_name: { type: 'string', required: true, },
    contact_person_1: { type: 'string', required: true, },
    contact_person_1_designation: { type: 'string', required: true, },
    contact_person_1_email: { type: 'string', required: true, },
    contact_person_1_phone: { type: 'string', required: true, },
    contact_person_2: 'string',
    contact_person_2_designation: 'string',
    contact_person_2_email: 'string',
    contact_person_2_phone: 'string',
    //items: { type: 'array', required: true, },
    team_id: 'string'

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};
sails.config.models.migrate='safe';

